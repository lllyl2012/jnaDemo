package top;

import com.sun.jna.Native;

public class JnaDemo {

    private static JnaSDK jnaSDK = (JnaSDK)(System.getProperty("os.name").toLowerCase().startsWith("win")? Native.loadLibrary("hello.dll",JnaSDK.class):Native.loadLibrary("hello.so",JnaSDK.class));

    public static void main(String[] args) {
        System.out.println(jnaSDK.Add(1,4));
    }
}
